<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "Legs : " . $sheep->sheeplegs . "<br>"; // 4
echo "Cold Blooded : " . $sheep->sheepcold_blooded . "<br><br>"; // "no"

$frog = new Frog("Buduk");

echo "Name : " . $frog->name . "<br>"; // "shaun"
echo "Legs : " . $frog->sheeplegs . "<br>"; // 4
echo "Cold Blooded : " . $frog->sheepcold_blooded . "<br>"; // "no"
echo "Jump : " . $frog->jump . "<br><br>";

$ape = new Ape("Kera Sakti");

echo "Name : " . $ape->name . "<br>"; // "shaun"
echo "Legs : " . $ape->sheeplegs . "<br>"; // 4
echo "Cold Blooded : " . $ape->sheepcold_blooded . "<br>"; // "no"
echo "Yell : " . $ape->yell . "<br>";

?>